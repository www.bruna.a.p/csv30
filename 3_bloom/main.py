#==========================================================================================================================================================
#                ██████████████████
#            ████░░░░░░░░░░░░░░░░░░████
#          ██░░░░░░░░░░░░░░░░░░░░░░░░░░██
#          ██░░░░░░░░░░░░░░░░░░░░░░░░░░██                Ghosts are terrible liars. You can see right through them!
#        ██░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░██                /
#        ██░░░░░░░░░░░░░░░░░░░░██████░░░░██               /
#        ██░░░░░░░░░░░░░░░░░░░░██████░░░░██    __________/
#        ██░░░░██████░░░░██░░░░██████░░░░██
#          ██░░░░░░░░░░██████░░░░░░░░░░██
#        ████░░██░░░░░░░░░░░░░░░░░░██░░████
#        ██░░░░██████████████████████░░░░██
#        ██░░░░░░██░░██░░██░░██░░██░░░░░░██
#          ████░░░░██████████████░░░░████
#        ██████████░░░░░░░░░░░░░░██████████
#      ██░░██████████████████████████████░░██
#    ████░░██░░░░██░░░░░░██░░░░░░██░░░░██░░████
#    ██░░░░░░██░░░░██████░░██████░░░░██░░░░░░██
#  ██░░░░████░░██████░░░░██░░░░██████░░████░░░░██
#  ██░░░░░░░░██░░░░██░░░░░░░░░░██░░░░██░░░░░░░░██
#  ██░░░░░░░░░░██░░██░░░░░░░░░░██░░██░░░░░░░░░░██
#    ██░░░░░░██░░░░████░░░░░░████░░░░██░░░░░░██
#      ████░░██░░░░██░░░░░░░░░░██░░░░██░░████
#        ██████░░░░██████████████░░░░██████
#          ████░░░░██████████████░░░░████
#        ██████████████████████████████████
#        ████████████████  ████████████████
#          ████████████      ████████████
#      ██████░░░░░░░░██      ██░░░░░░░░██████
#      ██░░░░░░░░░░████      ████░░░░░░░░░░██
#        ██████████              ██████████
#==========================================================================================================================================================
# Universidade Tecnológica Federal do Paraná
# Exercício:  implemente o efeito bloom em 2 versões:
#   - Filtragem Gaussiana
#   - Box Blur
# Alunos: Alan Jun Kowa Onnoda e Bruna Araújo Pinheiro
# Professor: Bogdan T. Nassu
#===============================================================================

import collections
import sys
import timeit
from typing import Dict
import numpy as np
import cv2
from numpy.core.numeric import roll

#===============================================================================

# imagem de entrada
INPUT_IMAGE =  'Wind Waker GC.bmp'

# filtro gaussiano
SIGMA = 13


#===============================================================================

''' 
Algoritmo Brigh pass

'''
def filtro_brightPass(img):
    img_bright = img.copy()
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY) 
    rows, cols, channels = img.shape
    for row in range(rows):
        for col in range(cols):
            if gray[row,col] < 0.5:
                img_bright[row,col] = 0
    return img_bright

''' 
Algoritmo Filtragem Gaussiana

'''
def filtro_gaussiano (img): 
    sigma_w = SIGMA
    rows, cols,channels = img.shape
    img_result = img.copy()
    while(sigma_w < rows and sigma_w < cols):

        img_cpy = cv2.GaussianBlur(img, (sigma_w,sigma_w),0)
        img_result = cv2.addWeighted(img_result, 1, img_cpy, 1, 0.0)
        sigma_w = sigma_w*2
        if not sigma_w % 2:
            sigma_w +=1 

    
    return img_result


''' 
Algoritmo Box Blur

'''
def filtro_blur(img, n, time):
    # borrando a imagem
    ksize = (int(1.5*SIGMA), int(1.5*SIGMA)) # tamanho do kernel (largura, altura)
    img_blur = cv2.blur(img, ksize) # borrando a imagem segundo os parametros
    # quantidade de vezes que a imagem foi borrada
    time += 1
    # verificando se precisamos borrar a imagem novamente
    if (time < n):
        img_blur += filtro_blur(img_blur, n, time)
    return img_blur

''' 
Algoritmo Bloom

'''
def filtro_bloom(img, filtro):
    # 1. passando um brigh pass
    img_bloom = filtro_brightPass(img)
    # 2. Borrando a máscara
    if(filtro == 'gaussiano'):
        img_bloom = filtro_gaussiano(img_bloom)
    if(filtro == 'blur'):
        img_bloom = filtro_blur(img_bloom, int(0.5*SIGMA), 0)
    # 3. Somando a mascara e a imagem original com uma soma ponderada
    img_bloom = cv2.addWeighted(img_bloom, 0.1, img, 1, 0.0)
    return img_bloom


#===============================================================================

def main ():
    
    # Abre a imagem
    img = cv2.imread (INPUT_IMAGE, cv2.IMREAD_COLOR)
    if img is None:
        print ('Erro abrindo a imagem.\n')
        sys.exit ()

    # É uma boa prática manter o shape com 3 valores, independente da imagem ser
    # colorida ou não. Também já convertemos para float32.
    #img = img.reshape ((img.shape [0], img.shape [1], 1)) # consideramos que a imagem é colorida
    img = img.astype (np.float32) / 255

    # Mantém uma cópia colorida para desenhar a saída.
    #img_out = cv2.cvtColor (img, cv2.COLOR_GRAY2BGR)

    # Filtragem gaussiana
    start_time = timeit.default_timer ()
    img_gaussiano = filtro_bloom ( img, 'gaussiano' )
    print ('Tempo algoritmo gaussiano: %f' % (timeit.default_timer () - start_time))
    cv2.imshow ('out - GAUSSIANO', img_gaussiano)
    cv2.imwrite ('out - GAUSSIANO.bmp', img_gaussiano*255)

    # Box blur
    start_time = timeit.default_timer ()
    img_blur = filtro_bloom ( img, 'blur' )
    print ('Tempo algoritmo box blur: %f' % (timeit.default_timer () - start_time))
    cv2.imshow ('out - BLUR', img_blur)
    cv2.imwrite ('out - BLUR.bmp', img_blur*255)


    cv2.waitKey ()
    cv2.destroyAllWindows ()



if __name__ == '__main__':
    main ()

#===============================================================================
