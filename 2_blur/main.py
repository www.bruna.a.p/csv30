#===============================================================================
#   　　　　/)─―ヘ       Qual o próximo número na sequência:
#   　　＿／　　　　＼      2, 10, 12, 16, 17, 18, 19, ...?
#   ／　　　  　●　　●    ／
#   ｜　　　　　　　▼　|       
#   ｜　　　　　　　亠ノ
#    U￣U￣￣￣￣U￣U
#===============================================================================
# Universidade Tecnológica Federal do Paraná
# Exercício: implementacao de 3 algoritmos para o filtro da média:
#   - Algoritmo “ingênuo”
#   - Filtro separável (com ou sem aproveitar as somas anteriores)
#   - Algoritmo com imagens integrais
# Alunos: Alan Jun Kowa Onnoda e Bruna Araújo Pinheiro
# Professor: Bogdan T. Nassu
#===============================================================================

import collections
import sys
import timeit
from typing import Dict
import numpy as np
import cv2
from numpy.core.numeric import roll

#===============================================================================

# imagem de entrada
INPUT_IMAGE =  'b01 - Original.bmp'

# a imagem é colorida?
IS_COLORED = 1

#===============================================================================

''' 
Algoritmo “ingênuo”

'''
def filtro_ingenuo (img, altura, largura): 
    img_media = np.empty(img.shape) # gerando uma imagem vazia
    rows, cols = img.shape
    altura_w = int(altura/2)
    largura_w = int(largura/2)
    for row in range (rows): # percorrendo a imagem
        for col in range (cols):
            soma = 0
            i = 0
            for row_w in range(row - altura_w , row + altura_w +1 ): # percorrendo a janela
                for col_w in range(col - largura_w , col + largura_w +1 ):
                    if ( ( row_w>=0 and row_w<rows ) and ( col_w>=0 and col_w<cols ) ):
                        soma += img[row_w, col_w]
                        i += 1
            img_media[row,col] = soma/i
    return img_media



''' 
Algoritmo separavel

'''
def filtro_separavel(img, altura, largura):
    img_horizontal = np.empty(img.shape) # gerando uma imagem vazia
    img_vertical = np.empty(img.shape)
    rows, cols = img.shape
    altura_w = int(altura/2)
    largura_w = int(largura/2)
    # filtro horizontal
    for row in range (rows):
        for col in range (cols):
            soma = 0
            i = 0
            for col_w in range(col - largura_w , col + largura_w +1 ): # janela largura
                if ( col_w>=0 and col_w<cols ):
                    soma += img[row, col_w]
                    i += 1
            img_horizontal[row,col] = soma/i
    # filtro vertical
    for row in range (rows):
        for col in range (cols):
            soma = 0
            i = 0
            for row_w in range(row - altura_w , row + altura_w +1 ): # janela altura
                if ( row_w>=0 and row_w<rows ):
                    soma += img_horizontal[row_w, col]
                    i += 1
            img_vertical[row,col] = soma/i
    return img_vertical


''' 
Algoritmo integral

'''
def filtro_integral (img, altura, largura): # aquele troço bizarro dos quadrados lá
    img_integral = np.empty(img.shape) # gerando uma imagem vazia
    img_aux = np.empty(img.shape) # imagem vazia para auxiliar nas contas
    rows, cols = img.shape
    altura_w = int(altura/2)
    largura_w = int(largura/2)
    for row in range (rows): # percorrendo a imagem
        for col in range (cols):
            img_integral[row, col] = img[row, col]
            if(col > 0): # não é a primeira coluna, soma com o da esquerda
                img_integral[row, col] += img_integral[row, col-1]
            if(row > 0): # não é a primeira linha, soma com o de cima
                img_integral[row, col] += img_integral[row-1, col]
            if(col > 0 and row > 0): # se está ali perdido no meio da imagem, subtrai o da diagonal esquerda porque ele está somado duas vezes
                img_integral[row, col] -= img_integral[row-1, col-1]

    for row in range(rows):
        for col in range(cols):
            soma = 0
            left = col - largura_w
            right = col + largura_w
            top = row - altura_w
            botton = row + altura_w
            if(left < 0):
               left = 0
            if(right>=cols):
                right = cols -1
            if(top < 0):
                top =0
            if(botton >= rows):
                botton = rows-1

            window_size = (botton - top)*(right - left)

            soma = img_integral[botton,right] - img_integral[top,right] - img_integral[botton,left] + img_integral[top,left]

            img[row,col] = soma/window_size
    return img


#===============================================================================

def main ():
    while(1):
        print('\nQual algoritmo você deseja testar?')
        print('1 - ingenuo')
        print('2 - separavel')
        print('3 - integrais')
        print('4 - Sair')
        num = input('>>')

        if(num != '4'):
            print('Selecione o tamanho da janela')
            altura = int(input('altura: '))
            largura = int(input('largura: '))
        
        # Abre a imagem em escala de cinza
        if(IS_COLORED):
            img = cv2.imread (INPUT_IMAGE, cv2.IMREAD_COLOR)
        if(not IS_COLORED):
            img = cv2.imread (INPUT_IMAGE, cv2.IMREAD_GRAYSCALE)
        if img is None:
            print ('Erro abrindo a imagem.\n')
            sys.exit ()
            
        # É uma boa prática manter o shape com 3 valores, independente da imagem ser
        # colorida ou não. Também já convertemos para float32.
        if(not IS_COLORED):
            img = img.reshape ((img.shape [0], img.shape [1], 1))
        img = img.astype (np.float32) / 255

        # Algoritmo ingenuo
        if(num == '1'):
            start_time = timeit.default_timer ()
            if(IS_COLORED):
                img[:, :, 0] = filtro_ingenuo ( img[:, :, 0], altura, largura ) # red
                img[:, :, 1] = filtro_ingenuo ( img[:, :, 1], altura, largura ) # green
                img[:, :, 2] = filtro_ingenuo ( img[:, :, 2], altura, largura ) # blue
            if(not IS_COLORED):
                img[:, :, 0] = filtro_ingenuo ( img[:, :, 0], altura, largura ) 
            print ('Tempo algoritmo ingenuo: %f' % (timeit.default_timer () - start_time))
            # imprimindo a imagem
            cv2.imshow ('out - INGENUO', img)
            cv2.imwrite ('out - INGENUO ' + str(largura) + 'x' + str(altura) + '.bmp', img*255)

        # Algoritmo separavel
        if(num == '2'):
            start_time = timeit.default_timer ()
            #img = filtroMedia_ingenuo (img, altura, largura)
            if(IS_COLORED):
                img[:, :, 0] = filtro_separavel ( img[:, :, 0], altura, largura ) # red
                img[:, :, 1] = filtro_separavel ( img[:, :, 1], altura, largura ) # green
                img[:, :, 2] = filtro_separavel ( img[:, :, 2], altura, largura ) # blue
            if(not IS_COLORED):
                img[:, :, 0] = filtro_separavel ( img[:, :, 0], altura, largura )
            print ('Tempo algoritmo separavel: %f' % (timeit.default_timer () - start_time))
            # imprimindo a imagem
            cv2.imshow ('out - SEPARAVEL', img)
            cv2.imwrite ('out - SEPARAVEL ' + str(largura) + 'x' + str(altura) + '.bmp', img*255)
        
        # Algoritmo integral
        if(num == '3'):
            start_time = timeit.default_timer ()
            if(IS_COLORED):
                img[:, :, 0] = filtro_integral ( img[:, :, 0], altura, largura ) # red
                img[:, :, 1] = filtro_integral ( img[:, :, 1], altura, largura ) # green
                img[:, :, 2] = filtro_integral ( img[:, :, 2], altura, largura ) # blue
            if(not IS_COLORED):
                img[:, :, 0] = filtro_integral ( img[:, :, 0], altura, largura ) 
            print ('Tempo algoritmo integral: %f' % (timeit.default_timer () - start_time))
            # imprimindo a imagem
            cv2.imshow ('out - INTEGRAL', img)
            cv2.imwrite ('out - INTEGRAL ' + str(largura) + 'x' + str(altura) + '.bmp', img*255)
        
        if(num == '4'):
            break
        
        
        cv2.waitKey ()
        cv2.destroyAllWindows ()


if __name__ == '__main__':
    main ()

#===============================================================================
