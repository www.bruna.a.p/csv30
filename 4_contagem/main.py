#===============================================================================
# Universidade Tecnológica Federal do Paraná
# Exercício: escreva um programa para estimar quantos grãos de arroz aparecem em cada uma das imagens dadas
#            usando os mesmos parametros para todas as imagens
# Alunos: Alan Jun Kowa Onnoda
# Professor: Bogdan T. Nassu
#===============================================================================

import collections
import sys
import timeit
from typing import Dict
import numpy as np
import cv2
from numpy.core.numeric import roll

#===============================================================================

# nome da imagem de entrada, sem a extensão
INPUT_IMAGE_NAME =  '60'

# TDO: ajuste estes parâmetros!
LIMIAR = 0.007
ALTURA_MIN = 1
LARGURA_MIN = 1
N_PIXELS_MIN = 30

#==============================================================================================================================================

''' 
Tratamento da imagem binarizada para melhor visualização dos dados
-Parâmetros:    -img: imagem de entrada
-Valor de retorno: versão tratada da imagem
'''
def tratamento (img):
    img_tratada = img.copy()
    img_tratada = erosao(img_tratada, 5, 5)
    img_tratada = dilatacao(img_tratada, 5, 5)
    return img_tratada

def dilatacao (img, altura, largura):
    img_dilatada = np.empty(img.shape) # tela em "branco"
    rows, cols, channels = img.shape
    # distancia entre o centro da janela e a borda
    altura_w = int(altura/2)
    largura_w = int(largura/2)
    # percorrendo a imagem
    for row in range (rows):
        for col in range (cols):
            # percorrendo a janela
            pixel_setado = 0
            for row_w in range(row - altura_w , row + altura_w +1 ):
                for col_w in range(col - largura_w , col + largura_w +1 ):
                    if ( ( row_w>=0 and row_w<rows ) and ( col_w>=0 and col_w<cols ) ):
                        if( img[row_w, col_w] == 1 ):
                            pixel_setado = 1
            if(pixel_setado):
                img_dilatada[row,col] = 1
    return img_dilatada

def erosao (img, altura, largura):
    img_erodida = np.empty(img.shape) # tela em "branco"
    rows, cols, channels = img.shape
    # distancia entre o centro da janela e a borda
    altura_w = int(altura/2)
    largura_w = int(largura/2)
    # percorrendo a imagem
    for row in range (rows):
        for col in range (cols):
            # percorrendo a janela
            n_pixels_setados = 0
            i = 0
            for row_w in range(row - altura_w , row + altura_w +1 ):
                for col_w in range(col - largura_w , col + largura_w +1 ):
                    if ( ( row_w>=0 and row_w<rows ) and ( col_w>=0 and col_w<cols ) ):
                        i += 1
                        if( img[row_w, col_w] == 1 ):
                            n_pixels_setados += 1
            if(n_pixels_setados == i):
                img_erodida[row,col] = 1
    return img_erodida

#Analisa o quantidade de pixels em cada blob, caso um blob tenha um tamanho maior que média + 2 desvios padrões,
# ele é considerado como um conjunto de objetos
def blob_list_analysis(list_blob):
    #calcular a média
    total_pixels = 0
    for blob in list_blob:
        total_pixels+=blob['n_pixels']

    media_pixels = total_pixels/len(list_blob)

    #calculo do desvio padrão
    sum_pixels = 0

    for blob in list_blob:
        sum_pixels += (blob['n_pixels']- media_pixels)**2

    desvio_pixels = np.sqrt(sum_pixels/len(list_blob))

    limiar_pixels = media_pixels + 2*desvio_pixels #limiar de seleção
    count = 0
    for blob in list_blob:
        count+=1
        # se o tamanho for maior que o limiar, calcula-se quantos objetos possíveis podem estar no blob
        if blob['n_pixels'] >= limiar_pixels: 
            blob["BigBlob"] = 1
            # print("Estimativa a partir dos desvios: ")
            # print((blob['n_pixels'] - media_pixels)/(2*desvio_pixels))
            count+=np.floor((blob['n_pixels'] - media_pixels)/(2*desvio_pixels))#quantidade de pixels a mais
    
    print("Objetos detectados de acordo com o desvio padrão: " + str(count))
    return 0


#-------------------------------------------------------------------------------
''' 
Binarização simples por limiarização.
-Parâmetros:    -img: imagem de entrada. Se tiver mais que 1 canal, binariza cada canal independentemente.
                -threshold: limiar.
-Valor de retorno: versão binarizada da img_in.
'''
def binarizacao (img, altura, largura):
    # primeiro uma media para dar uma sumida com uns ruidos chatos
    img_blurizada = cv2.blur(img, (9, 9))
    # agora uma limiarizacao adaptativa
    img_limiarizada = np.empty(img.shape) # tela em "branco"
    rows, cols = img_blurizada.shape
    # distancia entre o centro da janela e a borda
    altura_w = int(altura/2)
    largura_w = int(largura/2)
    # percorrendo a imagem
    for row in range (rows):
        for col in range (cols):
            # percorrendo a janela
            soma = 0
            n = 0
            for row_w in range(row - altura_w , row + altura_w +1 ):
                for col_w in range(col - largura_w , col + largura_w +1 ):
                    if ( ( row_w>=0 and row_w<rows ) and ( col_w>=0 and col_w<cols ) ):
                        n += 1
                        soma += img_blurizada[row_w, col_w]
            # pegando a media dos pixels da janela
            media = soma/n
            # se o valor do pixel menos a media for maior que o limiar (valor contrastando com a vizinhança local)    
            if( img_blurizada[row,col] - media > LIMIAR ):
                img_limiarizada[row,col] = 1
            else:
                img_limiarizada[row,col] = 0
    return img_limiarizada

#-------------------------------------------------------------------------------
'''
Rotulagem usando flood fill. Marca os objetos da imagem com os valores [0.1,0.2,etc].

Parâmetros: -img: imagem de entrada E saída.
            -largura_min: descarta componentes com largura menor que esta.
            -altura_min: descarta componentes com altura menor que esta.
            -n_pixels_min: descarta componentes com menos pixels que isso.

Valor de retorno: uma lista, onde cada item é um vetor associativo (dictionary)
com os seguintes campos:
    -'label': rótulo do componente.
    -'n_pixels': número de pixels do componente.
    -'T', 'L', 'B', 'R': coordenadas do retângulo envolvente de um componente conexo, respectivamente: topo, esquerda, baixo e direita.
'''

# função de inundação para auxilixar # EDIT HERE
def flood_fill(label, img, row, col, blob):
    img[row][col] = label
    blob["label"] = label # configurações do blob
    blob["n_pixels"] += 1
    if(row < blob["T"]):
        blob["T"] = row
    if(row > blob["B"]):
        blob["B"] = row
    if(col < blob["L"]):
        blob["L"] = col
    if(col > blob["R"]):
        blob["R"] = col
    height, width = img.shape[:2]
    if col > 0 and col < width - 2 and img[row][col-1] == 1.0:
        flood_fill(label, img, row, col-1,blob)
    if col > 0 and col < width - 2 and img[row][col+1] == 1.0:
        flood_fill(label, img, row, col+1,blob)
    if row > 0 and row < height - 2 and img[row+1][col] == 1.0:
        flood_fill(label, img, row+1, col,blob)
    if row > 0 and row < height - 2 and img[row+1][col] == 1.0:
        flood_fill(label, img, row+1, col,blob) 

# função do professor onde vamos usar flood fill (inundação)
def rotula (img, largura_min, altura_min, n_pixels_min):
    rows, cols, channels = img.shape
    list_blob = [] # lista a ser retornada
    label = 0.01
    for row in range (rows):
        for col in range (cols):
            if(img [row,col] == 1.0): # verificando se é um pixel branco ainda nâo pintado
                blob = {
                    "label" : 0,
                    "n_pixels": 0,
                    "T": row,
                    "L": col,
                    "B": row,
                    "R": col,
                    "BigBlob": 0
                }
                flood_fill(label, img, row, col,blob)
                if( blob["n_pixels"] > N_PIXELS_MIN ): # separa os grãos de arroz do ruído através do número de pixels
                    list_blob.append(blob)
                    #print('label: ' + str(round(label*100)))
                    #print('tamanho: ' + str(blob["n_pixels"]) + '\n')
                label = label + 0.01 # qundo ele voltar do flood fill iteramos a cor (label) para pegar o proximo
    
    #print(list_blob)
    blob_list_analysis(list_blob)
    return list_blob
    # TDO: escreva esta função.
    # Use a abordagem com flood fill recursivo.

#==========================================================================================================================================================

def main ():
    
    # Abrindo a imagem em escala de cinza
    img = cv2.imread (INPUT_IMAGE_NAME + '.bmp', cv2.IMREAD_GRAYSCALE)
    if img is None:
        print ('Erro abrindo a imagem.\n')
        sys.exit ()

    # É uma boa prática manter o shape com 3 valores, independente da imagem ser colorida ou não. Também já convertemos para float32.
    img = img.reshape ((img.shape [0], img.shape [1], 1))
    img = img.astype (np.float32) / 255
    img_out = cv2.cvtColor (img, cv2.COLOR_GRAY2BGR) # Mantém uma cópia colorida para desenhar a saída.

    # Binarização: Média seguida de Limiarização Adaptiva
    img = binarizacao (img, 7, 7)
    cv2.imshow (str(INPUT_IMAGE_NAME) + '_binarizada', img)
    cv2.imwrite (str(INPUT_IMAGE_NAME) + '_binarizada.png', img*255)

    #Tratamento (Morfologia)
    img = tratamento (img)
    cv2.imshow (str(INPUT_IMAGE_NAME) + '_tratada', img)
    cv2.imwrite (str(INPUT_IMAGE_NAME) + '_tratada.png', img*255)

    # Rotulagem com Flood Fill
    componentes = rotula (img, LARGURA_MIN, ALTURA_MIN, N_PIXELS_MIN)
    n_componentes = len (componentes)
    print ('componentes detectados segundo metodo antigo: %d' % n_componentes)

    # Mostra os objetos encontrados.
    for c in componentes:
        if c["BigBlob"]:
            cv2.rectangle (img_out, (c ['L'], c ['T']), (c ['R'], c ['B']), (0,1,0))
        else:
            cv2.rectangle (img_out, (c ['L'], c ['T']), (c ['R'], c ['B']), (0,0,1))
    cv2.imshow (str(INPUT_IMAGE_NAME) + '_out', img_out)
    cv2.imwrite (str(INPUT_IMAGE_NAME) + '_out.png', img_out*255)

    # Destruindo janelas
    cv2.waitKey ()
    cv2.destroyAllWindows ()


if __name__ == '__main__':
    main ()

#===============================================================================
