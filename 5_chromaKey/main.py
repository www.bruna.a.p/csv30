#==========================================================================================================================================================
# Universidade Tecnológica Federal do Paraná
# Exercício:  implemente o efeito bloom em 2 versões:
#   - Filtragem Gaussiana
#   - Box Blur
# Alunos: Alan Jun Kowa Onnoda e Bruna Araújo Pinheiro
# Professor: Bogdan T. Nassu
#===============================================================================

import collections
import sys
import timeit
from typing import Dict
import numpy as np
import cv2
from numpy.core.numeric import roll

#===============================================================================

# imagem de entrada
INPUT_IMAGE_BG =  'background.bmp'
INPUT_IMAGE_FG =  '3.bmp'

#===============================================================================

def createMask(img_fg):
    mask = img_fg.copy()
    rows, cols, channels = img_fg.shape
    for row in range (rows):
        for col in range (cols):
            #para ser verde o canal verde deve ser ligeiramente maior do que o azul e o vermelho, e assim temos a intensidade do verde
            if img_fg[row,col,1]>(img_fg[row,col,0] + 0.1) and img_fg[row,col,1]>(img_fg[row,col,2] + 0.1): # era 0.3
                mask[row,col] = 1-img_fg[row,col,1]
            # o que sobrar não é verde
            else:
                mask[row,col] = 1
            #mask[row,col] = 1-img_fg[row,col,1]
    return mask

def joinImages(bg,fg,mask):#juntar background com foreground usando a máscara
    rows, cols, channels = fg.shape
    res = np.zeros((rows,cols,3))
    bg = cv2.resize(bg,(cols,rows))
    for row in range (rows):
        for col in range (cols):
            if mask[row,col,0] == 1: # o corvo - branco na mascara
                res[row,col] = fg[row,col]
            elif mask[row,col,0] < 0.8: # chromakey - preto na imagem
                res[row,col] = bg[row,col]
            else: # transicao - tons de cinza na imagem
                res[row,col] = ((mask[row,col]+0.5)*fg[row,col] + (1-mask[row,col]+0.5)*bg[row,col]) / 2
    return res
#===============================================================================

def main ():
    
    # Abre a imagem
    img_bg = cv2.imread (INPUT_IMAGE_BG, cv2.IMREAD_COLOR)
    if img_bg is None:
        print ('Erro abrindo a imagem.\n')
        sys.exit ()

    # Abre a imagem
    img_fg = cv2.imread (INPUT_IMAGE_FG, cv2.IMREAD_COLOR)
    if img_fg is None:
        print ('Erro abrindo a imagem.\n')
        sys.exit ()

    # É uma boa prática manter o shape com 3 valores, independente da imagem ser
    # colorida ou não. Também já convertemos para float32.
    #img = img.reshape ((img.shape [0], img.shape [1], 1)) # consideramos que a imagem é colorida
    img_bg = img_bg.astype (np.float32) / 255
    img_fg = img_fg.astype (np.float32) / 255

    # Mantém uma cópia colorida para desenhar a saída.
    #img_out = cv2.cvtColor (img, cv2.COLOR_GRAY2BGR)
    #img_fg_blur = cv2.GaussianBlur(img_fg,(7,7),0)

    #Criar uma mascara para o chromaKey
    mask = createMask(img_fg)
    #mask = cv2.erode(mask,(11,11))
    cv2.imshow ('out - Mask', mask)

    res = joinImages(img_bg,img_fg,mask)
    cv2.imshow ('out' + INPUT_IMAGE_FG + ' - Result.bmp', res)
    cv2.imwrite ('out_' + INPUT_IMAGE_FG, res*255)

    cv2.waitKey ()
    cv2.destroyAllWindows ()


if __name__ == '__main__':
    main ()

#===============================================================================